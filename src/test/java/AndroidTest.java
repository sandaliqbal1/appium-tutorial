import static org.junit.Assert.assertEquals;

import java.time.Duration;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import com.google.common.base.Function;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class AndroidTest extends BaseTest {

	@Test
	public void testDragAndDrop() {
		waitForPresent(MobileBy.AccessibilityId("username"),3);
		driver.findElement(MobileBy.AccessibilityId("username")).clear();
		driver.findElement(MobileBy.AccessibilityId("username")).sendKeys("admin");
		driver.findElement(MobileBy.AccessibilityId("password")).clear();
		driver.findElement(MobileBy.AccessibilityId("password")).sendKeys("admin");
		driver.findElement(MobileBy.AccessibilityId("login")).click();
		waitForPresent(MobileBy.AccessibilityId("dragAndDrop"),3).click();
		MobileElement dropZone = waitForPresent(MobileBy.AccessibilityId("dropzone"),3);
		MobileElement dragMe = driver.findElement(MobileBy.AccessibilityId("dragMe"));
		new TouchAction(driver).press(new PointOption().point(dragMe.getLocation())).waitAction(new WaitOptions().withDuration(Duration.ofMillis(3000)))
				.moveTo(new PointOption<>().point(dropZone.getCenter())).release().perform();
		String expected = driver.findElement(MobileBy.AccessibilityId("success")).getText();
		assertEquals(expected, "Circle dropped");
	}
	
	public MobileElement waitForPresent(By locator, long timeInSec) {
		Wait<AppiumDriver<MobileElement>> wait = new FluentWait<AppiumDriver<MobileElement>>(
				driver).withTimeout(Duration.ofSeconds(timeInSec))
						.pollingEvery(Duration.ofMillis(1000)).ignoring(NoSuchElementException.class);

		MobileElement element = wait.until(new Function<AppiumDriver<MobileElement>, MobileElement>() {
			public MobileElement apply(AppiumDriver<MobileElement> driver) {
				return driver.findElement(locator);
			}
		});
		return element;
	}

}
